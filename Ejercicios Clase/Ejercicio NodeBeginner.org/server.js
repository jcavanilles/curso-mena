var http = require("http"); //Cargo modulo http
var url = require("url"); //Cargo modulo url

function iniciar(route,handle) { //creamos una funcion iniciar
  function onRequest(request, response) { //se crea una funcion Onrequest que se llama desde create server
    var pathname = url.parse(request.url).pathname; //Esto consigue la ruta sin dominio y sin parametros
    var dataPosteada = "";
    request.setEncoding("utf8"); 
    console.log("Petición para " + pathname + " recibida."); //pintamos el path name en la consola. 
   
   request.addListener("data", function(trozoPosteado) {  // Aqui añado un evento que va recogiendo poco a poco lo que se ha enviado por post.
          dataPosteada += trozoPosteado;
          console.log("Recibido trozo POST '" + trozoPosteado + "'.");
    });

    request.addListener("end", function() {
      route(handle, pathname, response, dataPosteada);// Una vez que ha terminado llamada a route definido en route.js
    }); 
  }

  http.createServer(onRequest).listen(8888); //Iniciamos el servidor llamando a Onrequest. 
  console.log("Servidor Iniciado.");
}

exports.iniciar = iniciar;