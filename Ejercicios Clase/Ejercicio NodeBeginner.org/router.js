function route(handle, pathname, response, postData) {
  console.log("A punto de rutear una peticion para " + pathname);
   //Comprobamos que en el objeto handle si la ruta es igual a funcion.  Si es funcion esta la url si no al else
  if (typeof handle[pathname] === 'function') {
    handle[pathname](response, postData); // Aqui invoca a la funcion definida en el objeto + los datos post. 
  } else {
  	// Si no hay ruta devuelve este error. 
    console.log("No hay manipulador de peticion para " + pathname);
    response.writeHead(404, {"Content-Type": "text/html"});
    response.write("404 No Encontrado");
    response.end();
  }
}

exports.route = route;