var server = require("./server"); //Cargo el modulo "server" que hemos creado
var router = require("./router"); // Cargo el modulo "router"
var requestHandlers = require("./requestHandlers"); // cargo el modulo RequestHandlers. 

var handle = {} // creo un objeto asociando rutas a funciones. 
handle["/"] = requestHandlers.iniciar;
handle["/iniciar"] = requestHandlers.iniciar;
handle["/subir"] = requestHandlers.subir;


// Salida en consola de Objeto Handle. . 
//{ '/': [Function: iniciar],
//  '/iniciar': [Function: iniciar],
//  '/subir': [Function: subir] }

server.iniciar(router.route, handle);//Llamamos a la funcion iniciar que esta en el archivo server.js
