/* ***Modulos *** */
const MongoClient = require('mongodb').MongoClient; // modulo conexion base de datos
const assert = require('assert'); // Modulo para errores
const ObjectID = require('mongodb').ObjectId; 
/* ***Fin Modulos *** */

/* ***Funciones *** */
// Funcion par insertar Estudiantes
var insertarEstudiantes = function (estudiantes,coleccion, callback){
coleccion.insertMany(estudiantes,function(err,res){
assert.equal(null, err);
console.log("Estudiantes insertadas correctamente. Se ha insertado "+res.result.n)
callback(res);
});
 }

 //Funcion buscar estudiantes (Documentos)
var buscarEstudiantes = function (consulta,coleccion, callback){
coleccion.find(consulta).toArray(function(err,res){
assert.equal(null, err);
console.log("Se encontraron los siguientes estudiantes:");
console.log(res);
console.log("   ");
callback(res);
});
 }

  //Funcion actualizar estudiantes (Documentos)
var actualizarEstudiantes = function (consulta,valor,coleccion, callback){

coleccion.updateMany(consulta,valor,function(err,res){
assert.equal(null, err);
console.log("Valores actualizados:"+res.result.n);
console.log("   ");
callback(res);
});
 }

   //Funcion actualizar estudiantes (Documentos)
var borrarEstudiantes = function (consulta,coleccion, callback){

coleccion.deleteMany(consulta,function(err,res){
assert.equal(null, err);
console.log("Valores Borrados:"+res.result.n);
console.log("   ");
callback(res);
});
 }

 var anadirIndice =function(coleccion,campo_indice,callback){
 	coleccion.createIndex(campo_indice,null,function(err,res){
assert.equal(null, err);
console.log("El indice ha quedado definido. ");
console.log("   ");
callback(res);
 	});

 }



/* ***FIN Funciones *** */



const url = 'mongodb://basededatos:Pass234!@ds237373.mlab.com:37373/mongodbconfunciones'; //Url BBDD
const dbName = 'mongodbconfunciones'; //Nombre Base de datos

MongoClient.connect(url,{useNewUrlParser: true}, function(err, client) { // Connecto. El objeto client representa la bbdd en caso conexion
	assert.equal(null, err); // En caso que la respuesta sea nula devuelve el error
    console.log("Se conecto correctamente");
    var db = client.db(dbName); //Genera o cambia a base de datos. 





// Json con estudiantes. 
    var estudiantes = [{name:"Juan",address:"Calle Mayor 37"}
,{name:"Javi",address:"Calle Mayor 38"}
,{name:"Sara",address:"Calle Mayor 39"}
,{name:"Layre",address:"Calle Mayor 40"}
,{name:"Susana",address:"Calle Mayor 41"}
,{name:"Angel",address:"Calle Mayor 42",telefono:914564545}
,{name:"Rosa",address:"Calle Mayor 43"}
,{name:"Blanca",address:"Calle Mayor 44"}
,{name:"Irene",address:"Calle Mayor 45"}
];

var coleccion =db.collection("estudiante"); //elijo la coleccion a modificar
// Llamo a la funcion insertar . Comentada para que no inserte mas.  
/*insertarEstudiantes(estudiantes,coleccion, function(){ //Llamo a la funcion pasando el Json, coleccion.
client.close();
})
*/

//busqueda por nombre empiecen por S
var consulta ={name:/^S/};
buscarEstudiantes(consulta, coleccion, function(){
	client.close();
})

//Consultar por un ID

var o_id= new ObjectID("5bcae22802e0d81a0083e855");
var consulta ={_id:o_id};
buscarEstudiantes(consulta, coleccion, function(){
	client.close();
})

//Consultar varios ID

var o_id= new ObjectID("5bcae22802e0d81a0083e855");
var o_id_2= new ObjectID("5bcae22802e0d81a0083e856");
var consulta ={_id: {$in: [o_id,o_id_2 ] } };
buscarEstudiantes(consulta, coleccion, function(){
	client.close();
})

//Consultar si un numero es mayor que 5

var consulta ={telefono: {$gt:5}};
buscarEstudiantes(consulta, coleccion, function(){
	client.close();
})



//Actualizamos registros que de nombre tengan "Javi". 

var consulta ={name:{$eq:"Javi"}};
var valor ={$set:{telefono:914565645} };
actualizarEstudiantes(consulta,valor,  coleccion, function(){
	client.close();
}); 

//Actualizamos registros que no tengan nombre. 

var consulta ={telefono:{"$exists":false}};
var valor ={$set:{telefono:611111111} };
actualizarEstudiantes(consulta,valor,  coleccion, function(){
	client.close();
}); 	


// Borramos una persona de la base de datos. 

var consulta ={name:"Rosa"};
borrarEstudiantes(consulta,coleccion, function(){
	client.close();
}); 


//Vamos a poner un campo como indice. 
campo_indice="name";
anadirIndice(coleccion,campo_indice, function(){
	client.close();
}); 


});




