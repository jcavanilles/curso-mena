var express = require("express");

var app =express();

//Middleware de  Static

//Con esto hace que todos los archivos de esa carpeta sean publicos sin tener que crear rutas.

app.use("/imgs",express.static('imgs'));


//tambien se puede asi y te ahorras las dos lineas de arriba. var app = require("express")();

// Con esto detectamos la ruta / y le devolvemos un hola mundo
//app.get("/", function(req,res){
//res.end("Hoola Mundo");
//});

// Aqui detectamos la ruta raiz / y te manda al HTML que hemos puesto. 
app.get("/", function(req,res){
res.sendFile(__dirname+"/html/index.html");
});

//Llamada al formulario. 

app.get("/formulario",function(req,res){

res.sendFile(__dirname+"/html/aplicacion.html");

});

app.get("/registro",function(req,res){

resultado ={
	nombre:req.query.nombre,
	apellidos:req.query.apellidos
};
console.log("Los datos son: "+JSON.stringify(resultado)+resultado.nombre+resultado.apellidos);
res.end("registrado en el sistema");


});

//Vamos a hacer un Midleware globar. Esto es ua funcion que se ejecuta de manera global y se ejecuta siempre que se llame a una pagina

app.use(function(req,res,next){
console.log("Iniciando middleware");
next();
});


//Middleware pero solo en una pagina

app.use("/lista_usaurios",function(req,res,next){
console.log("Middleware solo de la ruta");
next();
});

//middleware Local
function mi_function(req,res,next){
	console.log("Respuesta de mi funcion");
	next()
}

app.get("/middleware2",mi_function,function(req,res){

res.send("Ejecutando Middleware");
console.log("Middleware2");
});


//ejemplo ruta para obetener listado de usaurios. 

app.get("/lista_usuarios/:id_usuario/nombre/:nombre", function(req,res){

	console.log(req.params);
	res.write("ID: "+req.params.id_usuario);
	res.write("nombre:"+req.params.nombre);
	res.send();
console.log("Hay una peticion Get a lista usaurios con nombre e Id de usaurio");

});

app.get("/lista_usuarios", function(req,res){

	console.log("ID: "+req.query.id+"\n");
	console.log("Nombre: "+req.query.nombre+"\n");
	res.send("Se han pedido los usuarios"+req.query.id+" y nombre"+req.query.nombre);
	

});

app.post("/", function(req,res){
console.log("Se ha realizado una peticion Post a Raiz");
res.send("Peticion POST");
});

app.get("/*",function(req,res){

console.log("redirecctionando");
res.redirect(302,"/");
});





var server = app.listen(3000,"localhost",function(){
	var host = server.address().address;
	var puerto =server.address().port;
	console.log("El servidor esta funcionando correctamente "+ host+" en puerto"+puerto);
});

