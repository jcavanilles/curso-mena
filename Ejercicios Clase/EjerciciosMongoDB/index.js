const MongoClient = require('mongodb').MongoClient; // modulo conexion base de datos
const assert = require('assert'); // Modulo para errores
 
// Connection URL
const url = 'mongodb://basededatos:JavI0981@ds125352.mlab.com:25352/curso_javascript'; //Url BBDD
//ds125352.mlab.com:25352/curso_javascript
const dbName = 'curso_javascript'; //Nombre Base de datos
 

MongoClient.connect(url,{useNewUrlParser: true}, function(err, client) { // Connecto. El objeto client representa la bbdd en caso conexion
  assert.equal(null, err); // En caso que la respuesta sea nula devuelve el error
  // Seria lo mismo que hacer if(err) throw err
  console.log("Se conecto correctamente");
 
  var db = client.db(dbName); //Genera o cambia a base de datos. 

/*
// Crear la coleccion (antes llamabamos tablas) y dar nombre 
//Comentamos ya que esto solo habria que hacerlo una vez
db.createCollection("usuarios", function(err, res) {
  assert.equal(null, err); // En caso que la respuesta sea nula devuelve el error
  //console.log(err);
  //if (err) throw err;
console.log("Tabla usuarios creada!");
});

// Crear un objeto Json para insertar en la BBDD

var usuario = {name:"Pedro",address:"Calle Mayor 37"};

db.collection("usaurios").insertOne(usuario,function (err,res){
assert.equal(null, err);
console.log("Usaurio Insertado: "+usuario);
});


//Insertar Varios
var listausuario = [{name:"Pedro",address:"Calle Mayor 37"}
,{name:"Pedro2",address:"Calle Mayor 37"}
,{name:"Pedro3",address:"Calle Mayor 38"}
,{name:"Pedro4",address:"Calle Mayor 39"}
,{name:"Pedro5",address:"Calle Mayor 40"}
,{name:"Pedro6",address:"Calle Mayor 37",telefono:914564545}
,{name:"Pedro7",address:"Calle Mayor 37"}
,{name:"Pedro8",address:"Calle Mayor 37"}
,{name:"Pedro9",address:"Calle Mayor 37"}
,{name:"Pedro10",address:"Calle Mayor 37"}
,{name:"Pedro11",address:"Calle Mayor 37"}
,{name:"Pedro12",address:"Calle Mayor 37"}];

db.collection("usaurios").insertMany(listausuario,function(err,res){
assert.equal(null, err);
console.log("Numero de Usaurios Insertados: "+res.insertedCount);

});

*/
//Buscando documentos. Con findOne te encuentra el primero. 
var consulta ={name:"Pedro9"};
db.collection("usaurios").findOne(consulta,function(err,res){
	assert.equal(null, err);
	//Imprimir el valor de la consulta. 
	console.log("El Nombre es "+res.name+ " y la direccion es "+res.address);
})

//Buscando documentos. Varios
var orden = {name:1}; //ascendente -1 seria descendente
var consulta ={};
db.collection("usaurios").find(consulta).sort(orden).limit(55).toArray(function(err,res){
	assert.equal(null, err);
	//Imprimir el valor de la consulta. 
	console.log(res);
	console.log("Nombre "+res[0].name+" Direccion "+res[0].address)
})
/*

// Borrando elemento. 
var consulta={name:"Pedro11"};
db.collection("usaurios").deleteOne(consulta,function(err,res){
	assert.equal(null, err);
	console.log(consulta.name+" ha sido borrado")
})

// Borrando elementos (varios). 
var consult2={address:"Calle Mayor 37"};
db.collection("usaurios").deleteMany(consult2,function(err,res){
	assert.equal(null, err);
	console.log(res.result.n+" documentos Borrados")
})

 

// Actualizamos 1 elementos . 
var consulta={address:"Calle Mayor 38"};
var nuevosvalores={$set:{name:"Pedro 4", address:"Calle Lago Eyre 13"}}
db.collection("usaurios").updateOne(consulta,nuevosvalores,function(err,res){
	assert.equal(null, err);
	console.log(res.result.n+" documentos actualizdos")
})

// Actualizamos varios elementos . 
var consulta={name:"Pedro4"};
var nuevosvalores={$set:{name:"Juanito"}}
db.collection("usaurios").updateMany(consulta,nuevosvalores,function(err,res){
	assert.equal(null, err);
	console.log(res.result.n+" documentos actualizdos")
})

*/
//Actualizamos un campo.
//Cerramos base de datos. 
 client.close();
});