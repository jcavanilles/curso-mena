// Sin Callback

// Este codigo no tiene control de errores y devuelve un cascazo en la consola. 

//var fs=require("fs");
//var data=fs.readFileSync('input.txt');
//console.log(data.toString());
//console.log("program End");

// Con callback
// este codigo tiene control de errores y lo que hace es al no encontrar el archivo devolver los que has definido en el if. Si no devuelveel resto 
var fs=require("fs");
fs.readFile('inpu2t.txt',function(err,data){
	// Si falla esta disparando esta
	if(err) return console.error(err);
	//Si funciona devuevle esta.  ?=?=?=? Dudas
	console.log(data.toString());

});

console.log("program End");