var server = require("./rutearpeticiones_servidor"); //Cargo el modulo JS que hemos creado 
var router = require("./rutearpeticiones_route"); // Lo mismo en Route
var requestHandlers = require("./rutearpeticiones_handler");

var handle = {}
handle["/"] = requestHandlers.iniciar;
handle["/iniciar"] = requestHandlers.iniciar;
handle["/subir"] = requestHandlers.subir;


// El handle de arriba crea un objeto con esta pinta. 
//{ '/': [Function: iniciar],
//  '/iniciar': [Function: iniciar],
//  '/subir': [Function: subir] }

server.iniciar(router.route, handle);//Llamamos a la funcion iniciar que esta en el archivo JS
