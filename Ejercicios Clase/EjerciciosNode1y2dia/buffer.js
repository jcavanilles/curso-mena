// Creo un buffer de tamaño 26
buf = new Buffer(26);

//relleno la variable con todo el abecedario (se le suma 97 porque el codigo ascii empieza en 97)
for(var i=0; i<26;i++){
	buf[i]=i+97;
}

// salida Abecedario
console.log(buf.toString('ascii'));
//Salida abcef
console.log(buf.toString('ascii',0,5));
console.log(buf.toString('utf8',0,5));
console.log(buf.toString(undefined,0,5));


// buffer to Json
var json=buf.toJSON(buf);
console.log(json);


// Concatenar Bufferes
var buffer2=new Buffer("Esto es un texto");
var buffer3 = Buffer.concat([buf,buffer2]);
console.log(buffer3);