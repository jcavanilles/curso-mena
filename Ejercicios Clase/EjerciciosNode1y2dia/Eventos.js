// llamamos al modulo eventos
var events =require("events");
// creamos un objeto emisor de eventos
var eventEmitter = new events.EventEmitter();
//creamos una metodo con una funcion que devuelve un texto. 
var connectHandler = function connected(){
	console.log('connection succesful');
	//Emitimos un evento con el nombre data_received
eventEmitter.emit('data_received');
}

//Escuchamos los eventos y si hay uno que encuentra con nombre connection invoca la funciona connectHandler
eventEmitter.on('connection',connectHandler);
eventEmitter.on('data_received',function(){
	console.log('data received succesful');
});
eventEmitter.emit('connection');
console.log("Program Ended");