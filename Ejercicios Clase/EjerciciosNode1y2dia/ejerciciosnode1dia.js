// Modulo http servidor
var url=require('url');
var http=require('http');
var file=require('fs');
var events = require('events')
var datos_guardados= '';

// Servidor
http.createServer(function (req,res){
	//saco la url con la que se conectan con sus parametros
	var q = url.parse(req.url, true);
	console.log(q.query.param1); // pinto solo el path
	res.writeHead(200, {'Content-Type': 'text/html'}); 
	res.write(" He llegado hasta aqui");
    // Voy a leer el archivo y pinto los datos que encuentra

  file.readFile('ejemplo_archivo.html',function(err,data){
  //	res.writeHead(200, {'Content-Type': 'text/html'});
  	res.write(data);
    res.end();
  })
}).listen(8080);
  // Fuera de Servidor. 



var readerStream = file.createReadStream('ejemplo_archivo.html'); 
// Eventos
// Cuando recibe datos los guarda en la variable datos_Guardados.  
readerStream.on('data', function(caracter) { 
   datos_guardados += caracter; 
}); 

 readerStream.on('end',function(){ // Cuando termina pinta en la pantalla lo que ha guardado.
   console.log(datos_guardados); 
}); 

readerStream.on('error', function(err){ // si falla pinto el error en pantalla
   console.log(err.stack); 
}); 

readerStream.on('open', function () {  //  Cuando se abre muestro este texto en pantalla
console.log('El fichero se ha abierto'); 
}); 

console.log("Fin de lectura de datos");



// EVENTOS

var eventEmitter = new events.EventEmitter()

var alarma_horno = function () { 
  console.log('Pizza lista para comer'); 
} 
eventEmitter.on('alarma_lista', alarma_horno);

eventEmitter.emit('alarma_lista'); 



// Truncar un archivo. 

var buf = new Buffer(1024); //creamos un buffer
file.open('input.txt', 'r+', function(err, fd) { //abro el archivo
   if (err) {return console.error(err); }
   console.log("El archivo se ha abierto correctamente");
 
   // Truncate the opened file.
   file.ftruncate(fd, 10, function(err){ // Trunco el archivo y saco los primeros 10 caracteres. El archivo se corta. 
      if (err){ console.log(err); } 
      console.log("Archivo Truncada correctamente"); 
      
      file.read(fd, buf, 0, buf.length, 0, function(err, bytes){  //leo el archivo de nuevo ya cortado. 
         if (err){console.log(err);}

         if(bytes > 0){console.log(buf.slice(0, bytes).toString()); }

         // Close the opened file.
         file.close(fd, function(err){
            if (err){
               console.log(err);
            } 
            console.log("File closed successfully.");
         });
      });
   });
});


// Creo Directorio

file.mkdir('tmp',function(err){
   if (err) {
      return console.error(err);
   }
   console.log("Directorio creado correctamente.");
});
