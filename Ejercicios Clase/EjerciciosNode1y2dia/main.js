// Variable necesitada para crear un servidor. 
//En este caso llama al "modulo" http.  El modulo de Npm baja modulos.  
var http=require("http");

// Comando que crea un servidor. 
http.createServer(function(request,response){
	// Le mete una cabecera con un estado 200
	response.writeHead(200, {'Content-Type':'text/plain'});
	//Pone una linea
	response.write('Hooola2\n');
	//escribe un Hola mundo en la pantalla. La ultima tiene que ser con end.
	response.end('Hello World!\n');

}).listen(8080);
//Arriba termina el objeto y le define el puerto.
console.log("Server are running at 8080");
