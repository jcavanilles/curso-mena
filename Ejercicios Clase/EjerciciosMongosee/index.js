var mongoose = require('mongoose'); //Cargo modulo para Mongosee (Mongodb ya no hace falta)
var assert = require('assert');
var Author= require("./author");
var Book =require("./book");

mongoose.connect('mongodb://mongose-db:Pass1234!@ds237363.mlab.com:37363/mongosee',{ useNewUrlParser: true },function(err){
	assert.equal(null, err); 
	console.log("Conexion con BBDD realizada correctamente. ");

	//Crear nuevo autor

	var nuevoautor = new Author({
        _id: new mongoose.Types.ObjectId(),
        name: {
            firstName: 'Jamie',
            lastName: 'Munro'
        },
        biography: 'Jamie is the author of ASP.NET MVC 5 with Bootstrap and Knockout.js.',
        twitter: 'https://twitter.com/endyourif'
    });


	nuevoautor.save(function(err){
		assert.equal(null, err);
		console.log("Autor guardado correctamente");

		var nuevolibro = new Book({
			_id:new mongoose.Types.ObjectId(),
			Title:"El Quijote de la Mancha",
			author: nuevoautor.id
			});

		nuevolibro.save(function(err){
			assert.equal(null, err);
			console.log("Libro guardado correctamente");
			})

	console.log('Book successfully saved.');
});



});

