var fs = require('fs');
var path = require('path');

exports.send404 = function (response) {
	console.error("Resource not found");
	// Para el ALUMNO: Enviar 404 not found en caso de no encontrar el recurso
	// Enviar cabecera y mensaje al navegador(cliente)
	response.writeHead(404, {'Content-Type': 'text/plain'});
	response.end("Resource not found");
	}

exports.sendJson = function (data, response) { 
	// Para el ALUMNO: Enviar 200 OK en caso de no encontrar el recurso
	// Enviar cabecera (tipo JSON) y datos leidos al navegador(cliente)
	// Pista: usar JSON.stringify(data)
	response.writeHead(200, {'Content-Type': 'application/json'}); // Devuelvo 200 tipo Json
	console.log(data);
	data = JSON.stringify(data);
	response.end(data);
	}

exports.send500 = function (data, response) {
	console.error(data.red);
	response.writeHead(500, {'Content-Type': 'text/plain'});
	response.end(data);
	}

exports.staticFile = function (staticPath,data,response) {
var readStream;
data = data.replace(/^(\/home)(.html)?$/i,'$1.html');// Expresión regular para transformar /home en/home.html
data = '.' + staticPath + data;
console.log(data);
fs.stat(data, function (error, stats) { // Comprueba si el archivo existe
// Si no, enviar 404
if (error || stats.isDirectory()) {
	console.log(error);
return exports.send404(response);
}
readStream = fs.createReadStream(data);

readStream.on('open', function () {
	response.writeHead(200);
     readStream.pipe(response);
  });
readStream.on('error', function(err) {
return exports.send404(response);
  });





//PARA EL ALUMNO: código para generar la respuesta a enviar
// Tratar de enviar el fichero estático
// Crear stream de lectura de home.html
// Comprobar apertura de stream de lectura
// Envío dedatos del stream de lectura al cliente mediante 
// pipe()
// Comprobar errores en el stream de lectura
});
}