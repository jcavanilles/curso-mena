/* Modulos */
var http = require('http');
var employeeService = require('./lib/employees');
var responder = require('./lib/responseGenerator');
/* Fin Modulos */

var staticFile; //Representa el fichero estático a enviar al navegador
http.createServer(function (req, res) {
	var _url;// Url para usar en caso de que haya parámetros
	req.method = req.method.toUpperCase();// En caso de que el cliente use minúsculas para los métodos (Pasa de get a GET)
	console.log(req.method + ' ' + req.url);  //devuelve algo como "GET /employees/"

		if (req.method !== 'GET') { // Si el metodo no es GET le devuelve un 501
			res.writeHead(501, {'Content-Type': 'text/plain'});
			return res.end(req.method + ' is not implemented by this server.');
			}

		if (_url = /^\/employees\/$/i.exec(req.url)){
			employeeService.getEmployees(function (error, data) {
				if (error) {
					return responder.send500(error, res);// Enviar un error 500
				}
			return responder.sendJson(data, res);// send the data with a 200 status code
			});
			} 
		else if (_url = /^\/employees\/(\d+)$/i.exec(req.url)) {
			employeeService.getEmployee(_url[1], function (error, data) {// Encontrar el empleado por id en la ruta
				if (error) {
					return responder.send500(error, res);// send un error 500
				}
				if (!data) {
					return responder.send404(res);// Enviar un error 404 
				}

			return responder.sendJson(data,res);// Enviar los datos con status 200 ok
			});
			}
		else {
// Código para enviar el fichero estático
// Si no se encuentra debe mandarse un “404 not found” 
return responder.staticFile("/public", "/home", res);

//return responder.send404(res);// Enviar un error 404  
}
}).listen(1337,'127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');